<?php
    $base_url = "http://org_gen.test";

    session_start();
    $run = session_destroy();
    if($run == true){
        header('Location: ' . $base_url. '/login');
        exit;
    }
    else{
        // If the session destruction fails, redirect to the previous URL
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
    }
?>

