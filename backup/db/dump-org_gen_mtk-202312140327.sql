-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: org_gen_mtk
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `2372416_mtk`
--

DROP TABLE IF EXISTS `2372416_mtk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `2372416_mtk` (
  `id` int DEFAULT NULL,
  `nim` int NOT NULL,
  `nilai` char(1) DEFAULT NULL,
  `tanggal_nilai` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `2372416_mtk`
--

LOCK TABLES `2372416_mtk` WRITE;
/*!40000 ALTER TABLE `2372416_mtk` DISABLE KEYS */;
INSERT INTO `2372416_mtk` VALUES (2372416,1700018013,'B','3:01 - 14/Dec/2023');
/*!40000 ALTER TABLE `2372416_mtk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `5329266_mtk`
--

DROP TABLE IF EXISTS `5329266_mtk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `5329266_mtk` (
  `id` int DEFAULT NULL,
  `nim` int NOT NULL,
  `nilai` char(1) DEFAULT NULL,
  `tanggal_nilai` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `5329266_mtk`
--

LOCK TABLES `5329266_mtk` WRITE;
/*!40000 ALTER TABLE `5329266_mtk` DISABLE KEYS */;
INSERT INTO `5329266_mtk` VALUES (5329266,1700018013,'D','3:01 - 14/Dec/2023');
/*!40000 ALTER TABLE `5329266_mtk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `6374411_mtk`
--

DROP TABLE IF EXISTS `6374411_mtk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `6374411_mtk` (
  `id` int DEFAULT NULL,
  `nim` int NOT NULL,
  `nilai` char(1) DEFAULT NULL,
  `tanggal_nilai` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `6374411_mtk`
--

LOCK TABLES `6374411_mtk` WRITE;
/*!40000 ALTER TABLE `6374411_mtk` DISABLE KEYS */;
/*!40000 ALTER TABLE `6374411_mtk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `8452358_mtk_skp`
--

DROP TABLE IF EXISTS `8452358_mtk_skp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `8452358_mtk_skp` (
  `id` int DEFAULT NULL,
  `nim` int NOT NULL,
  `nilai` char(1) DEFAULT NULL,
  `tanggal_nilai` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `8452358_mtk_skp`
--

LOCK TABLES `8452358_mtk_skp` WRITE;
/*!40000 ALTER TABLE `8452358_mtk_skp` DISABLE KEYS */;
INSERT INTO `8452358_mtk_skp` VALUES (8452358,1700018013,'C','3:01 - 14/Dec/2023');
/*!40000 ALTER TABLE `8452358_mtk_skp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_pc`
--

DROP TABLE IF EXISTS `data_pc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_pc` (
  `nia` int NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `pc` varchar(20) NOT NULL,
  PRIMARY KEY (`pc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_pc`
--

LOCK TABLES `data_pc` WRITE;
/*!40000 ALTER TABLE `data_pc` DISABLE KEYS */;
INSERT INTO `data_pc` VALUES (44444444,'asdasd','Bidang Kader PC Djazman','kader'),(111112,'CEKKKKKKK INI KETUMa','Ketua Umum PC Djazmana','ketum');
/*!40000 ALTER TABLE `data_pc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `descmtk`
--

DROP TABLE IF EXISTS `descmtk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `descmtk` (
  `id` int NOT NULL,
  `A` varchar(300) NOT NULL,
  `B` varchar(300) NOT NULL,
  `C` varchar(300) NOT NULL,
  `D` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `descmtk`
--

LOCK TABLES `descmtk` WRITE;
/*!40000 ALTER TABLE `descmtk` DISABLE KEYS */;
INSERT INTO `descmtk` VALUES (2372416,'Al-Quran A','Al-Quran B','Al-Quran C','Al-Quran D'),(5329266,'ASD','ASD','ASD','ASD'),(6374411,'asd','asd','asd','asd'),(8452358,'SIKAP A','SIKAP B','SIKAP C','SIKAP D');
/*!40000 ALTER TABLE `descmtk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_data`
--

DROP TABLE IF EXISTS `form_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `form_data` (
  `judul` varchar(100) NOT NULL,
  `deskripsi` varchar(180) NOT NULL,
  `link` varchar(50) NOT NULL,
  `id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_data`
--

LOCK TABLES `form_data` WRITE;
/*!40000 ALTER TABLE `form_data` DISABLE KEYS */;
INSERT INTO `form_data` VALUES ('FORM PENGISIAN DJAZMAN','BAGI YANG MENDAFTAR SILAHKAN MENGISI FORM YANG SUDAH ADA','kader_form',9765);
/*!40000 ALTER TABLE `form_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idmtk`
--

DROP TABLE IF EXISTS `idmtk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `idmtk` (
  `id` int NOT NULL,
  `nama` varchar(30) NOT NULL,
  `semester` int NOT NULL,
  `thn` varchar(9) NOT NULL,
  `kelas` char(1) NOT NULL,
  `kkm` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idmtk`
--

LOCK TABLES `idmtk` WRITE;
/*!40000 ALTER TABLE `idmtk` DISABLE KEYS */;
INSERT INTO `idmtk` VALUES (2372416,'AL-QURAN',0,'','',''),(5329266,'EYY',0,'','',''),(6374411,'asdasd',0,'','','');
/*!40000 ALTER TABLE `idmtk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idmtk_skp`
--

DROP TABLE IF EXISTS `idmtk_skp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `idmtk_skp` (
  `id` int NOT NULL,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idmtk_skp`
--

LOCK TABLES `idmtk_skp` WRITE;
/*!40000 ALTER TABLE `idmtk_skp` DISABLE KEYS */;
INSERT INTO `idmtk_skp` VALUES (8452358,'SIKAP');
/*!40000 ALTER TABLE `idmtk_skp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instruktur`
--

DROP TABLE IF EXISTS `instruktur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `instruktur` (
  `nia` int NOT NULL,
  `jabatan` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `asal` varchar(20) NOT NULL,
  PRIMARY KEY (`nia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instruktur`
--

LOCK TABLES `instruktur` WRITE;
/*!40000 ALTER TABLE `instruktur` DISABLE KEYS */;
INSERT INTO `instruktur` VALUES (34234,'PO','asdasdasd','NBA'),(123123,'IOT','asdsd','FTI'),(1231232,'MOT','asdsd','FTI');
/*!40000 ALTER TABLE `instruktur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kader`
--

DROP TABLE IF EXISTS `kader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kader` (
  `nim` bigint NOT NULL,
  `komsat` varchar(20) NOT NULL,
  `namafull` varchar(50) NOT NULL,
  `namapanggil` varchar(20) NOT NULL,
  `notelp` varchar(15) NOT NULL,
  `tempat` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `web` varchar(150) NOT NULL,
  `hobi` varchar(200) NOT NULL,
  `motto` varchar(200) NOT NULL,
  `motivasi` varchar(200) NOT NULL,
  `bacaan` varchar(220) NOT NULL,
  `tanggal` date NOT NULL,
  `jk` varchar(2) NOT NULL,
  `penyakit` varchar(180) NOT NULL,
  `darah` varchar(2) NOT NULL,
  `prodi` varchar(150) NOT NULL,
  `fakultas` varchar(20) NOT NULL,
  `universitas` varchar(50) NOT NULL,
  `alamat` varchar(80) NOT NULL,
  `nama_ayah` varchar(50) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `kerja_ayah` varchar(50) NOT NULL,
  `kerja_ibu` varchar(50) NOT NULL,
  `essai` varchar(100) NOT NULL,
  `periode` varchar(20) NOT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kader`
--

LOCK TABLES `kader` WRITE;
/*!40000 ALTER TABLE `kader` DISABLE KEYS */;
INSERT INTO `kader` VALUES (1700018013,'HOS Cokroaminoto','hey kalian','aaaaaassssssss','085268043434','asd','asda@gmail.com','asds','asdsa','asdsad','asdads','Politik','1999-01-01','L','asdsad','B','asda','FAI','asdad','asd','asd','asd','asd','asd','','2020');
/*!40000 ALTER TABLE `kader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kader_catatan`
--

DROP TABLE IF EXISTS `kader_catatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kader_catatan` (
  `nim` int NOT NULL,
  `deskripsi` varchar(200) DEFAULT '',
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kader_catatan`
--

LOCK TABLES `kader_catatan` WRITE;
/*!40000 ALTER TABLE `kader_catatan` DISABLE KEYS */;
INSERT INTO `kader_catatan` VALUES (1700018013,'ssssss');
/*!40000 ALTER TABLE `kader_catatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kader_presensi`
--

DROP TABLE IF EXISTS `kader_presensi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kader_presensi` (
  `nim` int NOT NULL,
  `sakit` int DEFAULT NULL,
  `izin` int DEFAULT NULL,
  `tanpa_ket` int DEFAULT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kader_presensi`
--

LOCK TABLES `kader_presensi` WRITE;
/*!40000 ALTER TABLE `kader_presensi` DISABLE KEYS */;
INSERT INTO `kader_presensi` VALUES (1700018013,2,3,4),(1700018016,1,2,3);
/*!40000 ALTER TABLE `kader_presensi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `komsat`
--

DROP TABLE IF EXISTS `komsat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `komsat` (
  `id` int NOT NULL,
  `pelaksana` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `faks` int NOT NULL,
  `periode` varchar(15) NOT NULL,
  `ketua` varchar(50) NOT NULL,
  `telp` int NOT NULL,
  `cabang` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `komsat`
--

LOCK TABLES `komsat` WRITE;
/*!40000 ALTER TABLE `komsat` DISABLE KEYS */;
INSERT INTO `komsat` VALUES (1234567890,'PK IMM Farmasi','Jalan Prof. Dr. Soepomo S.H.a','Jalan Prof. Dr. Soepomo S.H.a','Umbulharjoa','Daerah Istimewa Yogyakartaaa',2111,'2019/2020a','Edy Susantoa',0,'Djazman Al Kindia');
/*!40000 ALTER TABLE `komsat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'org_gen_mtk'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-12-14  3:27:30
