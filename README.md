
# [Project] ORG-GEN

Project to manage a org cadre which divide in many classess and study subject, which in the end will be printed in xlsl / pdf file

## Properties :

* PHP 8.0 / 8.1
* No Framework / Self Managed
* HTML, CSS, JS
* Bootstrap 3.4.1
* JQuery 3.5.1
* Wording Language : Indonesian
* Tools old (netbeans), new (vscode)

## Features

1. Login
2. Logout
3. Cadre management (Note, Calculations, Personal Data, Present management)
4. Cadre present management
5. Subject study management
6. Behavioral subject study management
7. DAD data management
8. Admin control panel (Change name, Change Password, Change Email, Add new account, Public form management)
9. Print cadre data to xlsl / pdf

## Installation

1. Install laragon or any php managed system or you can try it manually
2. Install PHP (8.0 / 8.1)
3. Install Apache (2020 up)
4. Install MySQL (2017 up)
5. Run Apache
6. Run MySQL
7. Import / restore newest backup MySQL "org_gen_mtk" and "org_gen_user"
8. Run index.php using Apache

## Notes

* Design to run in only localhost / local computer
